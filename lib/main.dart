import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  @override
  Widget build(BuildContext context) {
    return const MaterialApp(
      home: HomePage(),
    );
  }
}

class HomePage extends StatefulWidget {
  const HomePage({super.key});

  @override
  State<HomePage> createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  late String id;
  late String email;
  late String name;
  @override
  void initState() {
    id = '';
    email = '';
    name = '';
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('HTTP GET'),
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Text('id ke : $id'),
            Text('email : $email'),
            Text('namanya : $name'),
            const SizedBox(
              height: 20,
            ),
            ElevatedButton(
                onPressed: () async {
                  var myResponse = await http
                      .get(Uri.parse('https://reqres.in/api/users/10'));
                  if (myResponse.statusCode == 200) {
                    print('GET DATA BERHASIL');
                    Map<String, dynamic> data =
                        json.decode(myResponse.body) as Map<String, dynamic>;
                    setState(() {
                      id = data['data']['id'].toString();
                      email = data['data']['email'].toString();
                      name =
                          '${data['data']['first_name']} ${data['data']['last_name']}';
                    });
                  } else {
                    // print('GET DATA GAGAL ${myResponse.statusCode}');
                    // setState(() {
                    //   body = 'GET DATA GAGAL ${myResponse.statusCode}';
                    // });
                  }
                },
                child: Text('GET'))
          ],
        ),
      ),
    );
  }
}
